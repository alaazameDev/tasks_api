<?php

use App\Http\Controllers\FrequencyController;
use App\Http\Controllers\SubTasksController;
use App\Http\Controllers\TasksController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// Tasks Resource
Route::get('summary', [FrequencyController::class, 'index']);
Route::apiResource('tasks', TasksController::class);
Route::apiResource('subtasks', SubTasksController::class);
Route::prefix('subtasks')->group(function () {
    Route::get('by-task-id/{taskId}', [SubTasksController::class, 'findSubTasksByTaskId']);
});
