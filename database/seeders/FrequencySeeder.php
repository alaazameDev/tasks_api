<?php

namespace Database\Seeders;

use App\Models\Frequency;
use Illuminate\Database\Seeder;

class FrequencySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $frequencies = ['Daily', 'Weekly', 'Monthly', 'Quarterly'];
        foreach ($frequencies as $freq) {
            Frequency::create(['name' => $freq]);
        }
    }
}
