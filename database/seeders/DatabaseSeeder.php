<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // \App\Models\User::factory(10)->create();

        // Seed Dummy User
        User::factory()->create([
            'name' => 'Test User',
            'email' => 'alaa.zamel80@gmail.com',
        ]);

        // Seed
        $this->call([
            FrequencySeeder::class
        ]);
    }
}
