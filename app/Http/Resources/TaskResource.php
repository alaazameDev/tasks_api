<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @property mixed id
 * @property mixed title
 * @property mixed instructions
 * @property mixed total_time
 * @property mixed frequency
 * @property mixed created_at
 * @property mixed subTasks
 */
class TaskResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'instructions' => $this->instructions,
            'total_time' => $this->total_time,
            'frequency' => $this->frequency,
            'sub_tasks' => SubTaskResource::collection($this->subTasks),
            'created_at' => $this->created_at
        ];
    }
}
