<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTaskRequest;
use App\Http\Resources\TaskResource;
use App\Http\Services\TasksService;
use App\Models\Task;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use JetBrains\PhpStorm\Pure;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class TasksController extends Controller
{

    private TasksService $tasksService;

    public function __construct(TasksService $tasksService)
    {
        $this->tasksService = $tasksService;
    }


    /**
     * Display a listing of the resource.
     */
    public function index(): AnonymousResourceCollection
    {
        $tasks = $this->tasksService->findAll();
        return TaskResource::collection($tasks);
    }

    #[Pure]
    public function show(Task $task): TaskResource
    {
        return new TaskResource($task);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(CreateTaskRequest $request): TaskResource
    {
        // validate request body
        $validated = $request->validated();
        try {
            // create the data
            $created = $this->tasksService->create($validated);
            return new TaskResource($created);
        } catch (Exception $exception) {
            throw new BadRequestHttpException();
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Task $task): JsonResponse
    {
        try {
            // delete the task
            $taskId = $task->getAttribute('id');
            $deleted = $this->tasksService->delete($taskId);
            return response()->json([
                "message" => $deleted > 0
                    ? "Item with id=" . $taskId . " deleted successfully"
                    : "No Entry was deleted"
            ]);
        } catch (Exception $exception) {
            throw new BadRequestHttpException();
        }
    }
}
