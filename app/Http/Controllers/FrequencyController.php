<?php

namespace App\Http\Controllers;

use App\Http\Resources\FrequencyResource;
use App\Http\Services\FrequencyService;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class FrequencyController extends Controller
{

    private FrequencyService $frequencyService;

    public function __construct(FrequencyService $frequencyService)
    {
        $this->frequencyService = $frequencyService;
    }

    public function index(): AnonymousResourceCollection
    {
        $frequencies = $this->frequencyService->findAll();
        return FrequencyResource::collection($frequencies);
    }
}
