<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSubTaskRequest;
use App\Http\Resources\SubTaskResource;
use App\Http\Services\SubTasksService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class SubTasksController extends Controller
{
    private SubTasksService $subTasksService;

    public function __construct(SubTasksService $subTasksService)
    {
        $this->subTasksService = $subTasksService;
    }

    public function findSubTasksByTaskId($taskId): AnonymousResourceCollection
    {
        $subTasks = $this->subTasksService->findSubTasksByTaskId($taskId);
        return SubTaskResource::collection($subTasks);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(CreateSubTaskRequest $request): SubTaskResource
    {
        $validated = $request->validated();

        try {
            $created = $this->subTasksService->create($validated);
            return new SubTaskResource($created);
        } catch (Exception $exception) {
            throw new BadRequestHttpException();
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id): JsonResponse
    {
        try {
            // delete the task
            $deleted = $this->subTasksService->delete($id);
            return response()->json([
                "message" => $deleted > 0
                    ? "Subtask with id=" . $id . " deleted successfully"
                    : "No Entry was deleted"
            ], status: $deleted > 0 ? 200 : 404);
        } catch (Exception $exception) {
            throw new BadRequestHttpException();
        }
    }
}
