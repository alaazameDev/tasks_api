<?php


namespace App\Http\Services;


use App\Models\SubTask;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class SubTasksService
{

    public function findSubTasksByTaskId($taskId): Collection|array
    {
        return SubTask::query()
            ->where('task_id', '=', $taskId)
            ->get();
    }

    /**
     * @throws Exception
     */
    public function create($data): Model|Builder
    {
        try {
            DB::beginTransaction();
            $created = SubTask::query()->create($data);
            DB::commit();
            return $created;
        } catch (Exception $ex) {
            DB::rollBack();
            throw $ex;
        }
    }

    /**
     * @throws Exception
     */
    public function delete($id)
    {
        try {
            DB::beginTransaction();
            $deleted = SubTask::query()
                ->where('id', '=', $id)
                ->delete();
            DB::commit();
            return $deleted;
        } catch (Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }
}
