<?php


namespace App\Http\Services;


use App\Models\Frequency;
use Illuminate\Database\Eloquent\Collection;

class FrequencyService
{

    public function findAll(): Collection
    {
        $frequencies = Frequency::with('tasks')->get();

        foreach ($frequencies as $frequency) {
            $taskIds = $frequency->tasks->pluck('id')->implode(',');
            $totalTime = '00:00';

            foreach ($frequency->tasks as $task) {
                $taskTotalTime = $task->totalTime;
                $taskTotalTimeInSeconds = strtotime($taskTotalTime);
                $totalTimeInSeconds = strtotime($totalTime);

                $totalTimeInSeconds += $taskTotalTimeInSeconds;
                $totalTime = gmdate('H:i', $totalTimeInSeconds);
            }

            $frequency->taskIds = $taskIds;
            $frequency->totalTime = $totalTime;
        }

        return $frequencies;
    }

}
