<?php


namespace App\Http\Services;


use App\Models\Task;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class TasksService
{

    /// Return all the tasks
    public function findAll(): Collection|array
    {
        return Task::with(['subTasks', 'frequency'])->get();
    }


    /**
     * @throws Exception
     */
    public function create($data): Model|Builder
    {
        try {
            DB::beginTransaction();
            // create a new entry
            $task = Task::query()->create($data);
            DB::commit();
            return $task;
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    /**
     * @throws Exception
     */
    public function delete($id)
    {
        try {
            DB::beginTransaction();
            $deleted = Task::query()->where('id', '=', $id)->delete();
            DB::commit();
            return $deleted;
        } catch (Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }

}
