<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property mixed subTasks
 */
class Task extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'instructions',
        'frequency_id'
    ];

    public function frequency(): BelongsTo
    {
        return $this->belongsTo(Frequency::class);
    }

    public function subTasks(): HasMany
    {
        return $this->hasMany(SubTask::class);
    }

    public function getTotalTimeAttribute(): string
    {
        $sum = 0;
        $strTime = strtotime('00:00:00');
        $durations = $this->subTasks()->pluck('duration');

        foreach ($durations as $duration) {
            $durationInSeconds = strtotime($duration) - $strTime;
            $sum += $durationInSeconds;
        }
        return gmdate('H:i', $sum);
    }
}
